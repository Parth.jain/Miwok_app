package com.example.android.miwok_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //find the view showing number Category
        TextView numbers=(TextView)findViewById(R.id.numbers);

        //passing this object names number as input on clickListener

        numbers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent=new Intent(MainActivity.this,NumbersActivity.class);
               startActivity(intent);
                Toast.makeText(v.getContext(),"Opening Number Activity",Toast.LENGTH_LONG).show();
            }
        });

        //finding view for family member
        TextView family=(TextView)findViewById(R.id.family);
        //binding onclickListener

        family.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,FamilyActivity.class);
                startActivity(intent);
                Toast.makeText(v.getContext(),"Opening Family activity",Toast.LENGTH_LONG).show();
            }
        });

        //find the textView of colors
        TextView color=(TextView)findViewById(R.id.colors);
        color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,ColorsActivity.class);
                startActivity(intent);
                Toast.makeText(v.getContext(),"Opening Color Activity",Toast.LENGTH_LONG).show();
            }
        });

        //finding the textview for phrase
        TextView phrase=(TextView)findViewById(R.id.phrases);

        //binding to the object
        phrase.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,PhrasesActivity.class);
                startActivity(intent);
                Toast.makeText(v.getContext(),"Opening Phrase View",Toast.LENGTH_LONG).show();

            }
        });
    }

  /*  public void openNumbersList(View view) {
        Intent i=new Intent(this,NumbersActivity.class);
        startActivity(i);
    }


*/
}
